import { Avatar as ChakraAvatar, WrapItem } from '@chakra-ui/react';
import { FC } from 'react';

interface AvatarProps {
  src: string;
  name?: string;
  size: string;
}

const Avatar: FC<AvatarProps> = (props) => {
  const { src, name, size = 'lg' } = props;
  return (
    <WrapItem>
      <ChakraAvatar size={size} name={name} src={src} />
    </WrapItem>
  );
};

export default Avatar;

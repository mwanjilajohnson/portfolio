import { fireEvent, render, screen } from '@testing-library/react';

import Button from '@/components/atoms/Button';

test('renders correctly', async () => {
  render(<Button label="Test label" />);

  expect(
    await screen.findByRole('button', { name: 'Test label' }),
  ).toBeInTheDocument();
});

test('fire event onclick', async () => {
  const onClick = jest.fn();

  render(<Button label="Test label" onClick={onClick} />);

  const button = await screen.findByRole('button', { name: 'Test label' });
  fireEvent.click(button);

  expect(onClick).toHaveBeenCalledTimes(1);
});

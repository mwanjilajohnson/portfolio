import { Button as ChakraButton } from '@chakra-ui/react';
import { FC } from 'react';

import Icons from '@/components/particles/icons';

interface ButtonProps {
  label?: string;
  size?: 'small' | 'large';
  variant?: 'primary' | 'secondary' | 'outline' | 'outlineDanger' | 'danger';
  icon?: 'arrowRight' | 'chevronRight' | 'sun' | 'moon';
  isLoading?: boolean;
  loadingText?: string;
  onClick?: () => void;
}

const Button: FC<ButtonProps> = (props) => {
  const { label, variant, size, icon, isLoading, loadingText, onClick } = props;

  const isSmall = size === 'small' ? 'sm' : 'md';

  return (
    <ChakraButton
      variant={variant}
      size={isSmall}
      rightIcon={icon && label ? Icons[icon] : undefined}
      isLoading={isLoading}
      loadingText={loadingText ? loadingText : undefined}
      onClick={onClick}
    >
      {label ? label : icon && Icons[icon]}
    </ChakraButton>
  );
};

export default Button;

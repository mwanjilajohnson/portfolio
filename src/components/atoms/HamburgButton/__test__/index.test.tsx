import { fireEvent, render, screen } from '@testing-library/react';

import HamburgerButton from '@/components/atoms/HamburgButton';

test('should display all the menu items when hamburger button is clicked', async () => {
  const items = [
    { label: 'About', href: '/about' },
    { label: 'Works', href: '/works' },
    { label: 'Posts', href: '/posts' },
    { label: 'Source', href: '/source' },
  ];
  render(<HamburgerButton items={items} />);

  const hamburgerButton = await screen.findByRole('button');
  fireEvent.click(hamburgerButton);

  expect(screen.getByText('About')).toBeInTheDocument();
  expect(screen.getByText('Works')).toBeInTheDocument();
  expect(screen.getByText('Posts')).toBeInTheDocument();
  expect(screen.getByText('Source')).toBeInTheDocument();
});

import { NavigationItemTypes } from '@/components/particles/types';
import { HamburgerIcon } from '@chakra-ui/icons';
import {
  Box,
  IconButton,
  Link,
  Menu,
  MenuButton,
  MenuItem,
  MenuList,
  useColorModeValue,
} from '@chakra-ui/react';
import { FC } from 'react';

import classes from './index.module.css';

interface HamburgerButtonProps {
  items: NavigationItemTypes[];
}

const HamburgerButton: FC<HamburgerButtonProps> = (props) => {
  const { items } = props;
  const isDark = useColorModeValue('light', 'dark');

  return (
    <Box display={{ base: 'inline-block', md: 'none' }}>
      <Menu isLazy id="navbar-menu">
        <MenuButton
          as={IconButton}
          icon={<HamburgerIcon />}
          variant="outline"
          aria-label="Options"
        />
        <MenuList>
          {items.map((item) => (
            <MenuItem
              as={Link}
              key={item.label}
              href={item.href}
              className={isDark === 'light' ? classes.menuItem : undefined}
            >
              {item.label}
            </MenuItem>
          ))}
        </MenuList>
      </Menu>
    </Box>
  );
};

export default HamburgerButton;

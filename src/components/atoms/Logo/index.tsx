import { Link, Text } from '@chakra-ui/react';
import NextLink from 'next/link';
import { FC } from 'react';

import Icons from '@/components/particles/icons';
import classes from './index.module.css';

const Logo: FC = () => {
  return (
    <Link as={NextLink} href="/" _hover={{ textDecoration: 'none' }}>
      <Text letterSpacing={'tighter'} className={classes.logo}>
        J{Icons.cup}hnson Mwakazi
      </Text>
    </Link>
  );
};

export default Logo;

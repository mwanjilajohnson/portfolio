import { Box, Heading, Text } from '@chakra-ui/react';
import { FC } from 'react';

interface ProfileProps {
  name: string;
  occupation: string;
}
const Profile: FC<ProfileProps> = (props) => {
  const { name, occupation } = props;
  return (
    <Box>
      <Heading size="lg">{name}</Heading>
      <Text>{occupation}</Text>
    </Box>
  );
};

export default Profile;

import { Center, Container, Text, useColorModeValue } from '@chakra-ui/react';
import { FC } from 'react';

import AnimatedDiv from '@/components/particles/AnimateDiv';

interface SummaryInfoProps {
  content: string;
}

const SummaryInfo: FC<SummaryInfoProps> = (props) => {
  const { content } = props;

  return (
    <AnimatedDiv delay={0.1}>
      <Container pt={14} maxW="container.sm">
        <Center
          bg={useColorModeValue(
            'rgba(255, 255, 255, 0.36)',
            'rgba(255, 255, 255, 0.08)',
          )}
          p="12px"
          css={{ backdropFilter: 'blur(10px)' }}
          color={useColorModeValue('#000', 'white')}
          rounded="5px"
        >
          <Text>{content}</Text>
        </Center>
      </Container>
    </AnimatedDiv>
  );
};

export default SummaryInfo;

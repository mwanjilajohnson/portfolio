import { render, screen } from '@testing-library/react';

import BioInfo from '@/components/molecules/BioInfo';

test('should display the correct content when rendered', () => {
  const items = {
    items: [
      {
        year: 2024,
        content: 'Test content',
      },
    ],
  };
  render(<BioInfo {...items} />);

  expect(screen.getByText('Test content')).toBeInTheDocument();
  expect(screen.getByText(2024)).toBeInTheDocument();
});

import { Box, Container, Heading } from '@chakra-ui/react';
import { FC } from 'react';

import AnimatedDiv from '@/components/particles/AnimateDiv';
import classes from './index.module.css';

interface Bio {
  year: number;
  content: string;
}
interface BioInfoProps {
  items: Bio[];
}
const BioInfo: FC<BioInfoProps> = (props) => {
  const { items } = props;
  return (
    <AnimatedDiv delay={0.3}>
      <Container maxW="container.sm" mt="24px">
        <Box>
          <Heading as="h3" fontSize={20} mb={4} variant="section-title">
            Bio
          </Heading>
        </Box>
        <Box className={classes['bio-section']}>
          {items.map((item) => (
            <Box key={item.year}>
              <span className={classes['bio-year']}>{item.year}</span>{' '}
              {item.content}
            </Box>
          ))}
        </Box>
      </Container>
    </AnimatedDiv>
  );
};

export default BioInfo;

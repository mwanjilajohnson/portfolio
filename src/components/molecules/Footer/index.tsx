import { Center, Container, Text } from '@chakra-ui/react';

const Footer = () => {
  const getYear = () => {
    const date = new Date();
    return date.getFullYear();
  };
  return (
    <Container maxW="container.sm" mt="24px" mb="24px">
      <Center>
        <Text color="grey">
          © {getYear()} Johnson Mwakazi. All Rights Reserved.
        </Text>
      </Center>
    </Container>
  );
};

export default Footer;

import { ChakraProvider } from '@chakra-ui/react';
import { render, screen } from '@testing-library/react';

import Hero from '@/components/molecules/Hero';

test('should display hero content when rendered', () => {
  render(
    <ChakraProvider>
      <Hero content="Test content" />
    </ChakraProvider>,
  );

  expect(screen.getByText('Test content')).toBeInTheDocument();
});

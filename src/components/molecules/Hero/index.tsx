import {
  Center,
  Container,
  Heading,
  useColorModeValue,
} from '@chakra-ui/react';
import { FC } from 'react';

interface HeroProps {
  content: string;
}

export const Hero: FC<HeroProps> = (props) => {
  const { content } = props;

  return (
    <Container
      display="flex"
      justifyContent="center"
      pt={14}
      maxW="container.sm"
    >
      <Center
        bg={useColorModeValue(
          'rgba(255, 255, 255, 0.36)',
          'rgba(255, 255, 255, 0.08)',
        )}
        p="70px"
        css={{ backdropFilter: 'blur(10px)' }}
        rounded="5px"
        mt="30px"
      >
        <Heading
          size="md"
          colorScheme={useColorModeValue('rgba(255, 255, 255, 0.08)', 'white')}
        >
          {content}
        </Heading>
      </Center>
    </Container>
  );
};

export default Hero;

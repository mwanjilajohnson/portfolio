import { render, screen } from '@testing-library/react';

import Likes from '@/components/molecules/Likes';

test('should display the correct content when rendered', () => {
  render(<Likes content="Test content" />);

  expect(screen.getByText('Test content')).toBeInTheDocument();
});

import { Box, Container, Heading, Text } from '@chakra-ui/react';
import { FC } from 'react';

import AnimatedDiv from '@/components/particles/AnimateDiv';

interface LikesProps {
  content: string;
}

const Likes: FC<LikesProps> = (props) => {
  const { content } = props;
  return (
    <AnimatedDiv delay={0.4}>
      <Container maxW="container.sm" mt="24px">
        <Box>
          <Heading as="h3" fontSize={20} mb={4} variant="section-title">
            I ♥
          </Heading>
          <Text>{content}</Text>
        </Box>
      </Container>
    </AnimatedDiv>
  );
};

export default Likes;

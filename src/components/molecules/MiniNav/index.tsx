import { ChevronRightIcon } from '@chakra-ui/icons';
import {
  Breadcrumb,
  BreadcrumbItem,
  BreadcrumbLink,
  Tag,
  Text,
} from '@chakra-ui/react';
import NextLink from 'next/link';
import { FC } from 'react';

interface MiniNavProps {
  linkLabel: string;
  linkUrl: string;
  projectName: string;
  startDate: string;
  completionDate: string;
}

const MiniNav: FC<MiniNavProps> = (props) => {
  const { linkLabel, linkUrl, projectName, startDate, completionDate } = props;
  return (
    <>
      <Breadcrumb
        spacing="8px"
        separator={<ChevronRightIcon color="gray.500" />}
      >
        <BreadcrumbItem>
          <BreadcrumbLink
            as={NextLink}
            href={`/${linkUrl}`}
            isCurrentPage
            color="primary"
          >
            {linkLabel}
          </BreadcrumbLink>
        </BreadcrumbItem>
        <Text as="strong">{projectName} </Text>{' '}
        <Tag ml="4px" size="sm">
          {startDate} - {completionDate}
        </Tag>
      </Breadcrumb>
    </>
  );
};

export default MiniNav;

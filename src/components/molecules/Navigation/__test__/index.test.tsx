import { render, screen } from '@testing-library/react';

import Navigation from '@/components/molecules/Navigation';

jest.mock('next/router', () => ({
  useRouter() {
    return {
      pathname: '',
    };
  },
}));

test('should display all the navigation menu', async () => {
  const items = [
    { label: 'About', href: '/about' },
    { label: 'Works', href: '/works' },
    { label: 'Posts', href: '/posts' },
    { label: 'Source', href: '/source' },
  ];
  render(<Navigation items={items} />);

  expect(screen.getByText('About')).toBeInTheDocument();
  expect(screen.getByText('Works')).toBeInTheDocument();
  expect(screen.getByText('Posts')).toBeInTheDocument();
  expect(screen.getByText('Source')).toBeInTheDocument();
});

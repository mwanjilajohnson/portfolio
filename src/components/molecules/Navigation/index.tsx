import { Box, HStack, Link } from '@chakra-ui/react';
import NextLink from 'next/link';
import { useRouter } from 'next/router';
import { FC } from 'react';

import { NavigationItemTypes } from '@/components/particles/types';

interface NavigationProps {
  items: NavigationItemTypes[];
}

const Navigation: FC<NavigationProps> = (props) => {
  const { items } = props;
  const router = useRouter();

  return (
    <HStack as="nav" spacing="2" display={{ base: 'none', md: 'flex' }}>
      {items.map((item) => (
        <Link
          key={item.label}
          as={NextLink}
          href={item.isExternal ? item.href : `/${item.href}`}
          isExternal={item.isExternal}
          bg={router.pathname === `/${item.href}` ? '#CAE6B2' : undefined}
          color={router.pathname === `/${item.href}` ? '#000' : undefined}
          p="2"
        >
          <HStack spacing={1}>
            <Box>{item.icon}</Box>
            <Box>{item.label}</Box>
          </HStack>
        </Link>
      ))}
    </HStack>
  );
};

export default Navigation;

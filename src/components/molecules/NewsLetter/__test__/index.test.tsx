import { render, screen } from '@testing-library/react';

import NewsLetter from '@/components/molecules/NewsLetter';

test('should display the correct content when rendered', () => {
  render(<NewsLetter content="Test content" />);

  expect(screen.getByText('Test content')).toBeInTheDocument();
  expect(
    screen.getByRole('button', { name: 'Sign up for a newsletter here' }),
  ).toBeInTheDocument();
});

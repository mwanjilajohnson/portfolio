import { Box, Center, Container, Heading, Text } from '@chakra-ui/react';
import { FC } from 'react';

import Button from '@/components/atoms/Button';
import AnimatedDiv from '@/components/particles/AnimateDiv';

interface NewsLetterProps {
  content: string;
}

const NewsLetter: FC<NewsLetterProps> = (props) => {
  const { content } = props;

  return (
    <AnimatedDiv delay={0.6}>
      <Container maxW="container.sm" mt="24px">
        <Box>
          <Heading as="h3" fontSize={20} mb={4} variant="section-title">
            Newsletter
          </Heading>
          <Text>{content}</Text>
          <Center>
            <Button label="Sign up for a newsletter here" variant="primary" />
          </Center>
        </Box>
      </Container>
    </AnimatedDiv>
  );
};

export default NewsLetter;

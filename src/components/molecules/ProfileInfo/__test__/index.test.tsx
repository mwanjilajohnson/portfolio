import { render, screen } from '@testing-library/react';

import ProfileInfo from '@/components/molecules/ProfileInfo';

test('should display the content when rendered', () => {
  render(
    <ProfileInfo
      name="Test name"
      occupation="Test occupation"
      avatarUrl="Test url"
    />,
  );

  expect(screen.getByText('Test name')).toBeInTheDocument();
  expect(screen.getByText('Test occupation')).toBeInTheDocument();
});

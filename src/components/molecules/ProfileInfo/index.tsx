import { Container, Spacer } from '@chakra-ui/react';
import { FC } from 'react';

import Avatar from '@/components/atoms/Avatar';
import Profile from '@/components/atoms/Profile';
import AnimatedDiv from '@/components/particles/AnimateDiv';

interface ProfileInfoProps {
  name: string;
  occupation: string;
  avatarUrl: string;
  size?: string;
}

const ProfileInfo: FC<ProfileInfoProps> = (props) => {
  const { name, occupation, avatarUrl, size = 'lg' } = props;
  return (
    <AnimatedDiv delay={0.1}>
      <Container display="flex" maxW="container.sm" mt="16px">
        <Profile name={name} occupation={occupation} />
        <Spacer />
        <Avatar src={avatarUrl} size={size} />
      </Container>
    </AnimatedDiv>
  );
};

export default ProfileInfo;

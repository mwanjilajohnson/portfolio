import { render, screen } from '@testing-library/react';

import SocialNetwork from '@/components/molecules/SocialNetwork';
import { FiGithub } from 'react-icons/fi';

test('should display the correct content when rendered', () => {
  const socials = [
    {
      icon: <FiGithub />,
      label: 'Github',
      url: 'githublink',
    },
  ];

  render(<SocialNetwork socials={socials} />);
  expect(screen.getByText('@Github')).toBeInTheDocument();
});

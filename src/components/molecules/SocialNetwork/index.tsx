import { Box, Button, Container, Heading } from '@chakra-ui/react';
import { FC, ReactElement } from 'react';

import AnimatedDiv from '@/components/particles/AnimateDiv';
import classes from './index.module.css';

interface SocialNetwork {
  icon: ReactElement;
  label: string;
  url: string;
}

interface SocialNetworkProps {
  socials: SocialNetwork[];
}

const SocialNetwork: FC<SocialNetworkProps> = (props) => {
  const { socials } = props;
  return (
    <AnimatedDiv delay={0.5}>
      <Container maxW="container.sm" mt="24px">
        <Box>
          <Heading as="h3" fontSize={20} mb={4} variant="section-title">
            On the web
          </Heading>
          {socials.map((socials) => (
            <Button
              as="a"
              key={socials.url}
              colorScheme="transparent"
              leftIcon={socials.icon}
              variant="link"
              href={socials.url}
              className={classes.btn}
              target="_blank"
              size="large"
              style={{ verticalAlign: 'middle' }}
            >
              @{socials.label}
            </Button>
          ))}
        </Box>
      </Container>
    </AnimatedDiv>
  );
};

export default SocialNetwork;

import WorkInfo from '@/components/molecules/WorkInfo';
import { render, screen } from '@testing-library/react';

jest.mock('next/router', () => ({
  useRouter() {
    return {
      pathname: '',
    };
  },
}));

test('should display correct content when rendered', () => {
  render(<WorkInfo content="Test content" />);

  expect(screen.getByText('Test content')).toBeInTheDocument();
});

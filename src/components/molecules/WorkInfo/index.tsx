import Button from '@/components/atoms/Button';
import AnimatedDiv from '@/components/particles/AnimateDiv';
import {
  Box,
  Center,
  Container,
  Heading,
  Highlight,
  Text,
  useColorModeValue,
} from '@chakra-ui/react';
import { useRouter } from 'next/router';
import { FC } from 'react';

interface WorkInfoProps {
  content: string;
}

const WorkInfo: FC<WorkInfoProps> = (props) => {
  const { content } = props;

  const router = useRouter();

  const navigateWorkPageHandler = () => {
    if (router.pathname !== '/works') {
      router.push('/works');
    }
  };

  return (
    <AnimatedDiv delay={0.2}>
      <Container display="flex" maxW="container.sm" mt="16px">
        <Box>
          <Heading as="h3" fontSize={20} mb={4} variant="section-title">
            Works
          </Heading>
          <Text>
            <Highlight
              query={[
                'React',
                'Next.js',
                'Javascript',
                'Typescript',
                'Node.js',
                'MongoDB',
                'DevOps CI/CD',
              ]}
              styles={{
                fontWeight: 'bold',
                color: useColorModeValue('black', 'white'),
              }}
            >
              {content}
            </Highlight>
          </Text>
          <Center>
            <Button
              label="My portfolio"
              variant="primary"
              icon="chevronRight"
              onClick={navigateWorkPageHandler}
            />
          </Center>
        </Box>
      </Container>
    </AnimatedDiv>
  );
};

export default WorkInfo;

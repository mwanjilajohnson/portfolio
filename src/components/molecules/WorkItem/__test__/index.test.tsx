import { render, screen } from '@testing-library/react';

import WorkItem from '@/components/molecules/WorkItem';

test('display image and description correctly', () => {
  render(
    <WorkItem
      name="Test name text"
      description="Test description text"
      imageUrl="test-image-url"
      altText="Test alt text"
    />,
  );
  expect(screen.getByText('Test name text')).toBeInTheDocument();
  expect(screen.getByText('Test description text')).toBeInTheDocument();
  expect(document.querySelector('img')).toHaveAttribute(
    'src',
    'test-image-url',
  );
  expect(document.querySelector('img')).toHaveAttribute('alt', 'Test alt text');
});

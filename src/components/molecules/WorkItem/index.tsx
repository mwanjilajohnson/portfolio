import { Card, CardBody, Heading, Image, Stack, Text } from '@chakra-ui/react';
import { FC } from 'react';

import AnimatedDiv from '@/components/particles/AnimateDiv';
import { WorkItemTypes } from '@/components/particles/types';
import classes from './index.module.css';

const WorkItem: FC<WorkItemTypes> = (props) => {
  const { name, description, imageUrl, altText } = props;

  return (
    <AnimatedDiv delay={0.4}>
      <Card
        maxW="sm"
        align="center"
        p={0}
        className={classes.card}
        size="sm"
        variant="filled"
      >
        <CardBody>
          <Image src={imageUrl} alt={altText} borderRadius="md" />
          <Stack mt="6" spacing="3" align="center">
            <Heading size="sm">{name}</Heading>
            <Text align="center">{description}</Text>
          </Stack>
        </CardBody>
      </Card>
    </AnimatedDiv>
  );
};

export default WorkItem;

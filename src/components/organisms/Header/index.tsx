import {
  Box,
  Container,
  Flex,
  HStack,
  Show,
  Spacer,
  useColorModeValue,
} from '@chakra-ui/react';
import { FC } from 'react';

import HamburgerButton from '@/components/atoms/HamburgButton';
import Logo from '@/components/atoms/Logo';
import ThemeSwitcher from '@/components/atoms/ThemeSwitcher';
import Navigation from '@/components/molecules/Navigation';
import { NavigationItemTypes } from '@/components/particles/types';

interface HeaderProps {
  items: NavigationItemTypes[];
}
const Header: FC<HeaderProps> = (props) => {
  const { items } = props;
  return (
    <Box
      position="fixed"
      as="nav"
      w="100%"
      bg={useColorModeValue('#ffffff40', '#20202380')}
      css={{ backdropFilter: 'blur(10px)' }}
      zIndex={2}
    >
      <Container display="flex" maxW="container.md">
        <Flex align="center" mr={5}>
          <Logo />
        </Flex>
        <Show above="md">
          <Navigation items={items} />
        </Show>
        <Spacer />
        <HStack spacing={0}>
          <ThemeSwitcher />
          <Show below="sm">
            <HamburgerButton items={items} />
          </Show>
        </HStack>
      </Container>
    </Box>
  );
};

export default Header;

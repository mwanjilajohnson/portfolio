import { Box, Container, Heading, SimpleGrid } from '@chakra-ui/react';
import { FC } from 'react';

import WorkItem from '@/components/molecules/WorkItem';
import AnimatedDiv from '@/components/particles/AnimateDiv';
import { WorkItemTypes } from '@/components/particles/types';

interface WorksProps {
  items: WorkItemTypes[];
}

const Works: FC<WorksProps> = (props) => {
  const { items } = props;

  return (
    <Container maxW="container.sm" mt="48px">
      <Box>
        <AnimatedDiv delay={0.2}>
          <Heading as="h3" fontSize={20} mb={4} variant="section-title">
            Works
          </Heading>
        </AnimatedDiv>
        <SimpleGrid columns={2}>
          {items.map((item) => (
            <WorkItem key={item.name} {...item} />
          ))}
        </SimpleGrid>
      </Box>
    </Container>
  );
};

export default Works;

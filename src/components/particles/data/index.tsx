import { FiGithub, FiGitlab, FiLinkedin } from 'react-icons/fi';

import imageUrl from '../../../../public/images/me.png';
import mjengosmartImageUrl from '../../../../public/images/mjengosmart.png';
import pbpImageUrl from '../../../../public/images/pbp.png';
import portfolioImageUrl from '../../../../public/images/portfolio.png';
import wakilichapchapImageUrl from '../../../../public/images/wakilichapchap.png';
import Icons from '../icons';
import { NavigationItemTypes, WorkItemTypes } from '../types';

export const header: NavigationItemTypes[] = [
  { label: 'About', href: '' },
  { label: 'Works', href: 'works' },
  { label: 'Posts', href: 'posts' },
  {
    label: 'Source',
    href: 'https://github.com/jmwakz99',
    isExternal: true,
    icon: Icons.github,
  },
];

export const hero = {
  content: 'Talk is cheap. Show code.',
};
export const profileInfo = {
  name: 'Johnson Mwakazi',
  occupation: 'Digital Craftsman ( Developer / Designer / DevOps CI/CD )',
  avatarUrl: imageUrl.src,
  size: 'xl',
};

export const summaryInfo = {
  content: 'Hello, Im an software developer based in Kenya!',
};

export const workInfo = {
  content:
    'Johnson is a dedicated Fullstack Web Developer specializing in React, Next.js, TypeScript, JavaScript, Node.js and MongoDB. With expertise in DevOps CI/CD, I ensure efficient and reliable software delivery. My skills in modern frameworks enable me to build dynamic, responsive, and scalable web applications. I am committed to continuous learning and delivering high-quality solutions that drive business success. In my free time, I love playing video games, traveling, and reading books.',
};

export const bioInfo = {
  items: [
    {
      year: 2024,
      content: 'Worked at Powered by people, Canada.',
    },
    {
      year: 2021,
      content: 'Worked at Friyay, United States.',
    },
    {
      year: 2019,
      content: 'Worked at Wakili ChapChap, Kenya.',
    },
    {
      year: 2017,
      content: 'Worked at MjengoSmart, Kenya.',
    },
    {
      year: 1993,
      content: 'Born in taita taveta, Kenya.',
    },
  ],
};

export const likes = {
  content: 'Machine Learning, Video games, Traveling, Reading books',
};

export const socialNetwork = {
  socials: [
    {
      icon: <FiLinkedin />,
      label: 'johnsonmwakazi',
      url: 'https://www.linkedin.com/in/johnson-mwakazi/',
    },
    {
      icon: <FiGithub />,
      label: 'jmwakz99',
      url: 'https://github.com/jmwakz99',
    },
    {
      icon: <FiGitlab />,
      label: 'mwanjilajohnson',
      url: 'https://gitlab.com/mwanjilajohnson',
    },
  ],
};

export const newsLetter = {
  content:
    'Join me through my journey as I share my thoughts on software development, design, and DevOps. Subscribe to my newsletter to stay updated on the latest trends and best practices in the industry.',
};

export const works: WorkItemTypes[] = [
  {
    name: 'Powered by People',
    description:
      'A website built for Powered by People, that enables independent, diverse makers and brands by providing access to markets, financing, and digital tools.',
    imageUrl: pbpImageUrl.src,
    altText: 'Powered by people site',
  },
  {
    name: 'Wakili ChapChap',
    description:
      'A legal self help platform tailored for SMEs and Individuals to create their legal documents online with ease.',
    imageUrl: wakilichapchapImageUrl.src,
    altText: 'Wakili ChapChap site',
  },

  {
    name: 'MjengoSmart',
    description:
      'A website built for MjengoSmart, that brings structured processes, guidelines and management in the construction industry by creating a construction ecosystem that is transparent and efficient.',
    imageUrl: mjengosmartImageUrl.src,
    altText: 'MjengoSmart Site',
  },
  {
    name: 'Portfolio',
    description:
      'A portfolio website built to showcase my skills and projects I have worked on.',
    imageUrl: portfolioImageUrl.src,
    altText: 'My Portfolio Site',
  },
];

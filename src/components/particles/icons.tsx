import { ReactElement } from 'react';
import {
  FiArrowRight,
  FiChevronRight,
  FiCoffee,
  FiGitMerge,
  FiMoon,
  FiSun,
} from 'react-icons/fi';

interface IconType {
  arrowRight: ReactElement;
  chevronRight: ReactElement;
  sun: ReactElement;
  moon: ReactElement;
  cup: ReactElement;
  github: ReactElement;
}

const Icons: IconType = {
  arrowRight: <FiArrowRight />,
  chevronRight: <FiChevronRight />,
  sun: <FiSun />,
  moon: <FiMoon />,
  cup: <FiCoffee />,
  github: <FiGitMerge />,
};

export default Icons;

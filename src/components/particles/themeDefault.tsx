import { mode } from '@chakra-ui/theme-tools';

export const themeDefault = {
  config: {
    initialColorMode: 'light',
    useSystemColorMode: false,
  },
  styles: {
    global: (props: { colorMode: string }) => ({
      body: {
        bg: mode('#f0e7db', '#202023')(props),
        fontFamily: '"Quicksand", sans-serif',
        fontOpticalSizing: 'auto',
      },
    }),
  },
  colors: {
    brand: {
      400: '#41bbd9',
      500: '#2C7865',
    },
    primary: '#007F73',
    secondary: '#2D9596',
    black: '#222831',
    white: '#FEFBF6',
    danger: '#F03E3E',
  },
  components: {
    Heading: {
      variants: {
        'section-title': {
          textDecoration: 'underline',
          fontSize: 20,
          textUnderlineOffset: 6,
          textDecorationColor: '#525252',
          textDecorationThickness: 4,
          marginTop: 3,
          marginBottom: 4,
        },
      },
    },
    Button: {
      baseStyle: {
        borderRadius: '0.375rem',
        lineHeight: '1.2',
        fontWeight: 600,
        color: 'white',
        verticalAlign: 'sub',
        margin: '0.5rem',
      },
      sizes: {
        sm: {
          height: '2rem',
          fontSize: '0.875rem',
        },
        md: {
          height: '2.5rem',
          fontSize: '1rem',
        },
      },
      variants: {
        primary: {
          bg: 'primary',
          _hover: {
            bg: 'secondary',
          },
        },
        secondary: {
          bg: 'secondary',
          _hover: {
            bg: 'primary',
          },
        },
        danger: {
          bg: 'danger',
        },

        outlineDanger: {
          border: '1px solid',
          borderColor: 'danger',
          color: 'danger',
        },
      },
      icons: {
        color: 'white',
        fontColor: 'white',
        bg: 'primary',
      },
    },
  },
};

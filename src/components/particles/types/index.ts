export interface NavigationItemTypes {
  label: string;
  href: string;
  icon?: JSX.Element;
  isExternal?: boolean;
}

export interface WorkItemTypes {
  name: string;
  description: string;
  imageUrl: string;
  altText: string;
}

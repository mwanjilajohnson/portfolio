import { FC } from 'react';

import Footer from '@/components/molecules/Footer';
import Hero from '@/components/molecules/Hero';
import Header from '@/components/organisms/Header';
import Works from '@/components/organisms/Works';
import {
  NavigationItemTypes,
  WorkItemTypes,
} from '@/components/particles/types';

interface WorksTemplateProps {
  items: WorkItemTypes[];
  hero: {
    content: string;
  };
  header: NavigationItemTypes[];
}

const WorksTemplate: FC<WorksTemplateProps> = (props) => {
  const { items, hero, header } = props;

  return (
    <>
      <Header items={header} />
      <Hero {...hero} />
      <Works items={items} />
      <Footer />
    </>
  );
};

export default WorksTemplate;

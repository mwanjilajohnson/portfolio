import { Center, Container, Heading } from '@chakra-ui/react';
import { useRouter } from 'next/router';
import { TfiFaceSad } from 'react-icons/tfi';

import Button from '@/components/atoms/Button';
import AnimatedDiv from '@/components/particles/AnimateDiv';

const PageNotFound = () => {
  const router = useRouter();

  const navigateHomeHandler = () => {
    if (router.pathname !== '/') {
      router.push('/');
    }
  };

  return (
    <AnimatedDiv delay={0.1}>
      <Container maxW="container.sm">
        <Center pt="200px" display="flex" flexDirection="column">
          <TfiFaceSad size={50} />
          <Heading as="h3" size="lg" mt="8px">
            Page is under construction.
          </Heading>
          <Center mt="16px">
            <Button
              label="Go back to home"
              variant="primary"
              onClick={navigateHomeHandler}
            />
          </Center>
        </Center>
      </Container>
    </AnimatedDiv>
  );
};

export default PageNotFound;

import { ChakraProvider, extendTheme } from '@chakra-ui/react';
import type { AppProps } from 'next/app';

import Fonts from '@/components/particles/Fonts';
import { themeDefault } from '@/components/particles/themeDefault';
import { AnimatePresence } from 'framer-motion';

const defaultTheme = extendTheme({ ...themeDefault });

if (typeof window !== 'undefined') {
  window.history.scrollRestoration = 'manual';
}

export default function App({ Component, pageProps }: AppProps) {
  return (
    <ChakraProvider theme={defaultTheme}>
      <Fonts />
      <AnimatePresence
        mode="wait"
        initial={true}
        onExitComplete={() => {
          if (typeof window !== 'undefined') {
            window.scrollTo({ top: 0 });
          }
        }}
      >
        <Component {...pageProps} />
      </AnimatePresence>
    </ChakraProvider>
  );
}

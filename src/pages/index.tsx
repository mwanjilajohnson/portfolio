import SummaryInfo from '@/components/atoms/SummaryInfo';
import BioInfo from '@/components/molecules/BioInfo';
import Footer from '@/components/molecules/Footer';
import Hero from '@/components/molecules/Hero';
import Likes from '@/components/molecules/Likes';
import NewsLetter from '@/components/molecules/NewsLetter';
import ProfileInfo from '@/components/molecules/ProfileInfo';
import SocialNetwork from '@/components/molecules/SocialNetwork';
import WorkInfo from '@/components/molecules/WorkInfo';
import Header from '@/components/organisms/Header';
import {
  bioInfo,
  header,
  hero,
  likes,
  newsLetter,
  profileInfo,
  socialNetwork,
  summaryInfo,
  workInfo,
} from '@/components/particles/data';

const Home = () => {
  // const miniNav = {
  //   linkLabel: 'Works',
  //   linkUrl: 'works',
  //   projectName: 'Portfolio',
  //   startDate: '2021',
  //   completionDate: '2021',
  // };
  return (
    <>
      <Header items={header} />
      <Hero {...hero} />
      {/* <MiniNav {...miniNav} /> */}
      <SummaryInfo {...summaryInfo} />
      <ProfileInfo {...profileInfo} />
      <WorkInfo {...workInfo} />
      <BioInfo {...bioInfo} />
      <Likes {...likes} />
      <SocialNetwork {...socialNetwork} />
      <NewsLetter {...newsLetter} />
      <Footer />
    </>
  );
};
export default Home;

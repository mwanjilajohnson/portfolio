import { header, hero, works } from '@/components/particles/data';
import WorksTemplate from '@/components/templates/WorksTemplate';

const Works = () => {
  return (
    <>
      <WorksTemplate hero={hero} items={works} {...hero} header={header} />
    </>
  );
};

export default Works;
